﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace ActiveCampaign.Net.Models.Contact
{
    public class BasicContactInfo
    {
        public int Id { get; set; }

        [Required]
        public string Email { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string Phone { get; set; }

        public string OrganizationName { get; set; }

        public List<string> Tags { get; set; }

        public string IPAddress { get; set; }

        public List<Field> Fields { get; set; }

        public int FormId { get; set; }
    }
}