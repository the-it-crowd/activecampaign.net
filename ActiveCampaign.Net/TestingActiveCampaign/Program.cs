﻿using ActiveCampaign.Net.Services;

namespace TestingActiveCampaign
{
    using System;
    using System.Collections.Generic;

    class Program
    {
        static void Main(string[] args)
        {

            const string apiUrl = "";
            const string apiKey = "";

            const string resellerApiUrl = "";
            const string resellerApiKey = "";

            var accountService = new AccountService(resellerApiKey, resellerApiUrl);

            var accounCancel = accountService.AccountCancel("activehosted.com", "Because it's test");
            Console.WriteLine(@"Code: " + accounCancel.ResultCode);
            Console.WriteLine(@"Message: " + accounCancel.ResultMessage);


            ////Contact Sync ---------------------------------------------------------------------------
            //var contactservice = new ContactService(apiKey, apiUrl);

            //var selectedActiveCampaignListsIds = new List<BasicContactList>
            //{
            //    new BasicContactList()
            //    {
            //        Id = 117, 
            //        Status = SubscriptionStatus.Unsubscribed, 
            //        Noresponders = false,
            //        SubscribeDate = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"),
            //        InstantResponders = false, 
            //        LastMessage = false
            //    }
            //};

            //var fieldsList = new List<Field>()
            //{
            //    new Field() {Name = "%TESTMULTILINESELECT%", Value = "Option 1, Option 2"} , 
            //    new Field() {Name = "%TESTDATA%", Value = "2015-05-05"}, 
            //    new Field() {Name = "%TESTMULTILINETEXT%", Value = "Lorem ipsum"},
            //    new Field() {Name = "%TESTRADIO%", Value = "Option 1"}, 
            //    new Field() {Name = "%TESTSELECT%", Value = "Option 2"}, 
            //    new Field() {Name = "%TESTTEXT%", Value = "Lorem ipsum......"}, 
            //    new Field() {Name = "%TESTCHECKBOX2%", Value = "Option 1, Option 2, Option 3"}, 
            //};

            //var contact = new BasicContactInfo()
            //{
            //    Email = "test@example.org",
            //    FirstName = "First Name",
            //    LastName = "Last Name",
            //    Phone = "+12 345 678 900",
            //    OrganizationName = "Acme inc.",
            //    Tags = new List<string>() {"Api", "WOW", "Much Tags"},
            //    FormId = 2681,
            //    Fields = fieldsList,
            //};

            //var syncContact = contactservice.SyncContact(contact, selectedActiveCampaignListsIds);
            //Console.WriteLine(@"Code: " + syncContact.ResultCode);
            //Console.WriteLine(@"Message: " + syncContact.ResultMessage);
            //Console.WriteLine(@"ContactId: " + syncContact.SubscriberId);
            ////Contact Sync End ---------------------------------------------------------------------------



            Console.ReadKey();

        }
    }
}
