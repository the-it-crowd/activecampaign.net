# **Install** #
Install Activecampaign.net via NuGet 

```
#!
Install-Package ActiveCampaign.Net -Pre
```
# **Word from author** #
Keep in mind that package is still in progress. You can see list of available methods below.

# **Services** #
[Account methods](#markdown-header-account-service)

[Contact methods](#markdown-header-contact-service)


## **Account service** ##
--------
### Adding an account
Add a new account, just like you would on the Manage Accounts page of the reseller panel. 
```
#!c#

var ac = new AccountService("apiKey", "apiUrl");

var account =  new AccountAdd()
{
    AccountName = "mynewaccount",
    Cname = "ac.exmaple.org",
    Email = "jon@example.org",
    Notification = "jane@example.org",
    PlanId = 1,
    Language = "english",
    Timezone = "America/Chicago"
};
AccountAddResponse result = ac.AccountAdd(account);
```
### Cancel account
Allows you to cancel an active account. 
```
#!c#
var ac = new AccountService("apiKey", "apiUrl");

var accountName = "account.activehosted.com";
var reason = "Reasone of canceling account";
Result result = ac.AccountCancel(accountName, reason);
```
### Accounts list
View multiple accounts under your reseller profile including all information associated with each. 
```
#!c#
var ac = new AccountService("apiKey", "apiUrl");

var search = "myexampleaccount";
AccountListResponse result = ac.AccountList(search);
```

### Account name check
Allows you to check remotelly if the account name is already taken.

```
#!c#

var ac = new AccountService("apiKey", "apiUrl");

var accountNameToCheck = "myexampleaccount";
Result result = ac.AccountNameCheck(accountNameToCheck);
```

### Account plans
Allows you to retrieve a list of currently available plans for an account.

```
#!c#

var ac = new AccountService("apiKey", "apiUrl");

var accountName = "myexampleaccount";
AccountPlansResponse result = ac.AccountPlans(accountName);
```

### Account status
Allows you to check the account status. Possible results are active, disabled, creating, cancelled. 

```
#!c#

var ac = new AccountService("apiKey", "apiUrl");

var accountName = "myexampleaccount";
AccountStatusResponse result = ac.AccountStatus(accountName);
```


## **Contact service** ##
--------

### Contact sync
Add or edit a contact based on their email address. Instead of calling contact_view to check if the contact exists, and then calling contact_add or contact_edit, you can make just one call and include only the information you want added or updated.

```
#!c#

var ac = new ContactService("apiKey", "apiUrl");

BasicContactInfo contact = new BasicContactInfo
{
	Email = "jonh@example.org",
	FirstName = "John",
	LastName = "Doe",
	Tags = new List<string> { "TAG" },
	Phone = "+1 234 567 890",
	Fields = new List<Field>
	{
		new Field { Name = "%FIELD_NAME%", Value = "Value", Id = 1 }
	}
};

List<BasicContactList> selectedActiveCampaignListsIds = new List<BasicContactList>
{
	new BasicContactList
	{
		Id = 1,
		Status = SubscriptionStatus.Active,
		Noresponders = false,
		InstantResponders = false,
		LastMessage = false
	}
};

ContactSyncResponse result = ac.SyncContact(contact, selectedActiveCampaignListsIds);
```